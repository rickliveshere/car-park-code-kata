var format = require('string-format');
var Calculator = require('../src/calculator');

describe("The program should report", function() {
  
  const OVER_CAPACITY_OUTPUT = 'The car park is over-capacity';
  const PROFIT_OUTPUT = 'The car park made £{0} in profit';
  const LOSS_OUTPUT = 'The car park lost £{0}';

  var calculator = new Calculator();

  it("over-capacity if more vehicles enter than available spaces", function() {
    var vehicles = {
      halfDay: 50,
      day: 143,
      month: 8,
      year: 50
    };

    expect(calculator.report(vehicles)).toBe(OVER_CAPACITY_OUTPUT);
  });

  it("over-capacity if more than 8 vehicles use the reserved month spaces", function() {
    var vehicles = {
      halfDay: 1,
      day: 1,
      month: 9,
      year: 1
    };

    expect(calculator.report(vehicles)).toBe(OVER_CAPACITY_OUTPUT);
  });

  it("over-capacity if more than 50 vehicles use the reserved yearly spaces", function() {
    var vehicles = {
      halfDay: 1,
      day: 1,
      month: 1,
      year: 51
    };

    expect(calculator.report(vehicles)).toBe(OVER_CAPACITY_OUTPUT);
  });

  it("profit when total income is greater than total cost", function() {
    var vehicles = {
      halfDay: 50,
      day: 142,
      month: 8,
      year: 50
    };

    expect(calculator.report(vehicles)).toBe(format(PROFIT_OUTPUT, 264.44));
  });

  it("loss when total income is less than total cost", function() {
    var vehicles = {
      halfDay: 50,
      day: 5,
      month: 8,
      year: 50
    };

    expect(calculator.report(vehicles)).toBe(format(LOSS_OUTPUT, -283.56));
  });
});