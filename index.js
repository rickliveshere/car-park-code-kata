"use strict";

var Calculator = require('./src/calculator');

var calculator = new Calculator();

var vehicles = {
  halfDay: 50,
  day: 142,
  month: 8,
  year: 50
};

var income = calculator.getTotalIncomeForDay(vehicles);

console.log('Vehicle total: ' + calculator.getVehicleTotal(vehicles));
console.log('Total income: ' + income);
console.log('Total costs: ' + calculator.getTotalCostsForDay(income));
console.log('Output: ' + calculator.report(vehicles));