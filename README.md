# Car park kata

The owner of a car park would like to forecast how much profit they are making. The car park has a total capacity of 300 spaces.

## Tariffs
Different tariffs are available depending upon length of stay:

 - £3 - half a day
 - £5 - full day
 - £150 - month
 - £1750 - year

8 reserved spaces are available only to monthly ticket holders.
50 reserved spaces are avaiable only to yearly ticket holders.

## Costs
As like any other business, the car park also has operating costs - both variable and fixed. A summary of these costs are as follows:

###### VAT
20% per day
###### Lighting
£1500 per month
###### Staff
£18000 per month

Objective
-------
The car park owner wants a program to forecast how much profit (or loss) they could make in a *single day*.

The program will take a single json object as input, containing both the amount of cars and a count of each tariff in a given day.

As output, the program will show:

###### For profit
```sh
'The car park made £x.xx in profit'
```
###### For loss
```sh
'The car park lost £x.xx'
```

It should not be possible for the car park to be over-capacity. If this situation occurs, the program will output:


```sh
'The car park is over-capacity'
```


##### Assumptions
  - A month is 30 days
  - Vehicles stay for the full duration of their tariff
  - Profit or loss is reported to the nearest 2 decimal digits