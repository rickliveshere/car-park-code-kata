exports.tariffs = {
	halfDay: 3,
	day: 5,
	month: 150,
	year: 1750
};

exports.costs = {
	vatPerDay: 0.2, // percentage
	lightingPerMonth: 1500,
	staffPerMonth: 18000
};

exports.spaces = {
	total: 300,
	reserved: {
		year: 50,
		month: 8
	}
};