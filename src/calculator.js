"use strict";

var data = require('./data');
var format = require('string-format');

const DAYS_IN_MONTH = 30;
const MONTHS_IN_YEAR = 12;

const OVER_CAPACITY_OUTPUT = 'The car park is over-capacity';
const PROFIT_OUTPUT = 'The car park made £{0} in profit';
const LOSS_OUTPUT = 'The car park lost £{0}';

let Calculator = function() { };

Calculator.prototype.getVehicleTotal = function(vehicles) {
  	let dayTotal = (vehicles.halfDay * 2) + vehicles.day;

  	return dayTotal + vehicles.month + vehicles.year;
};

Calculator.prototype.getTotalCostsForDay = function(income) {
  	let vat = income * data.costs.vatPerDay;
  	let lighting = data.costs.lightingPerMonth / DAYS_IN_MONTH;
  	let staff = data.costs.staffPerMonth / DAYS_IN_MONTH;

  	return vat + lighting + staff;
};

Calculator.prototype.getTotalIncomeForDay = function(vehicles) {
	let halfDayIncome = vehicles.halfDay * data.tariffs.halfDay;
	let dayIncome = vehicles.day * data.tariffs.day;
	let monthIncome = (vehicles.month * data.tariffs.month) / DAYS_IN_MONTH;
	let yearIncome = ((vehicles.year * data.tariffs.year) / MONTHS_IN_YEAR) / DAYS_IN_MONTH;

	return halfDayIncome + dayIncome + monthIncome + yearIncome;
};

Calculator.prototype.isOverCapacity = function(vehicles) {
  	let vehicleTotal = this.getVehicleTotal(vehicles);

  	return data.spaces.total < vehicleTotal
  		|| data.spaces.reserved.year < vehicles.year
  		|| data.spaces.reserved.month < vehicles.month;
};

Calculator.prototype.report = function(vehicles) {
  	if (this.isOverCapacity(vehicles))
  		return OVER_CAPACITY_OUTPUT;

  	let income = this.getTotalIncomeForDay(vehicles);
  	let costs = this.getTotalCostsForDay(income);

  	let profit = income - costs;

  	if (profit >= 0)
  		return format(PROFIT_OUTPUT, profit.toFixed(2));

  	return format(LOSS_OUTPUT, profit.toFixed(2));
};

module.exports = Calculator;